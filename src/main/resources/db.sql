CREATE TABLE account(
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    PRIMARY KEY (username)
);
INSERT INTO account VALUES('David','bachem123');
INSERT INTO account VALUES('Christiano','ronaldo123');


CREATE TABLE message(
    id INT NOT NULL AUTO_INCREMENT,
    owner VARCHAR(50) NOT NULL,
    createdOn DATE NOT NULL,
    personal BOOLEAN NOT NULL,
    content VARCHAR(500) NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO message(owner,createdOn,personal,content) VALUES('David', NOW(), TRUE, 'I hate socker');

INSERT INTO message(owner,createdOn,personal,content) VALUES('David', CURDATE(), FALSE, 'I am a vagitarian');

INSERT INTO message(owner,createdOn,personal,content) VALUES('Christiano', CURDATE(), FALSE, 'Hi everyone');

INSERT INTO message(owner,createdOn,personal,content) VALUES('Christiano', CURDATE(), TRUE, 'I am drunk');

