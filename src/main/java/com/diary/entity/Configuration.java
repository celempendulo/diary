package com.diary.entity;

import javax.json.bind.annotation.JsonbProperty;

public class Configuration
{
    private String url;
    private String user;
    private String password;
    private boolean authenticate;

    public Configuration()
    {

    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public void setAuthenticate(boolean authenticate)
    {
        this.authenticate = authenticate;
    }

    @JsonbProperty(nillable = true, value = "password")
    public String getPassword()
    {
        return password;
    }

    @JsonbProperty(value = "url")
    public String getUrl()
    {
        return url;
    }

    @JsonbProperty(nillable = true, value = "user")
    public String getUser()
    {
        return user;
    }

    @JsonbProperty(value = "authenticate")
    public boolean shouldAuthenticate()
    {
        return authenticate;
    }
}
