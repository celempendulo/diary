package com.diary.entity;

import javax.json.bind.annotation.JsonbProperty;
import java.util.Date;

public class Message
{
    private String owner;
    private Date createdOn;
    private boolean personal;
    private String content;
    private String id;

    public Message(String owner, Date createdOn, boolean personal, String content, String id)
    {
        this.owner = owner;
        this.createdOn = createdOn;
        this.personal = personal;
        this.content = content;
        this.id = id;
    }

    public Message()
    {

    }

    public Date getCreatedOn()
    {
        return createdOn;
    }

    @JsonbProperty(nillable = true, value = "owner")
    public String getOwner()
    {
        return owner;
    }

    public String getContent()
    {
        return content;
    }

    @JsonbProperty(value = "personal")
    public boolean isPersonal()
    {
        return personal;
    }

    @JsonbProperty(nillable = true, value = "id")
    public String getId()
    {
        return id;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public void setCreatedOn(Date createdOn)
    {
        this.createdOn = createdOn;
    }

    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public void setPersonal(boolean personal)
    {
        this.personal = personal;
    }

    public void setId(String id) 
    {
        this.id = id;
    }
    
    
}
