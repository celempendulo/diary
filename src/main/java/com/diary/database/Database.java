package com.diary.database;

import com.diary.entity.Account;
import com.diary.entity.Configuration;
import com.diary.entity.Message;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class Database
{

    private static Database database;

    private final Connection connection;

    private Database(Configuration configuration) throws ClassNotFoundException, SQLException
    {
        Class.forName("com.mysql.jdbc.Driver");
        if(configuration.shouldAuthenticate())
        {
            connection = DriverManager.getConnection(configuration.getUrl(), configuration.getUser(),
                    configuration.getPassword());
        }
        else
        {
            connection = DriverManager.getConnection(configuration.getUrl());
        }

    }

    public String addMessage(Message message) throws SQLException
    {
        PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO message (owner,createdOn,personal,content) VALUES (?,?,?,?);");
        statement.setString(1, message.getOwner());
        statement.setDate(2, new Date(message.getCreatedOn().getTime()));
        statement.setBoolean(3, message.isPersonal());
        statement.setString(4, message.getContent());
        statement.execute();
        
        ResultSet rs = connection.prepareStatement("SELECT LAST_INSERT_ID();").executeQuery();
        rs.next();
        return rs.getString(1);
    }

    public boolean deleteMessage(String id) throws SQLException
    {
        PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM message WHERE id=?");
        statement.setString(1, id);
        return statement.execute();
    }

    public Message getMessage(String id) throws SQLException
    {
        PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM message WHERE id=?");
        statement.setString(1, id);
        ResultSet rs = statement.executeQuery();

        if(rs.next())
        {
            return new Message(
                    rs.getString("owner"),
                    rs.getDate("createdOn"),
                    rs.getBoolean("personal"),
                    rs.getString("content"),
                    rs.getString("id"));
        }
        return null;
    }

    public boolean updateMessage(String id, String content) throws SQLException
    {
        PreparedStatement statement = connection.prepareStatement(
                "UPDATE message set content=? WHERE id=?");
        statement.setString(1, content);
        statement.setString(2, id);
        return statement.execute();
    }

    public boolean updateMessage(String id, boolean personal) throws SQLException
    {
        PreparedStatement statement = connection.prepareStatement(
                "UPDATE message set personal=? WHERE id=?");
        statement.setBoolean(1, personal);
        statement.setString(2, id);
        return statement.execute();
    }

    public List<Message> getUserMessages(String owner) throws SQLException
    {
        List<Message> messages = new ArrayList<>();
        PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM message WHERE owner=?");
        statement.setString(1, owner);
        ResultSet rs = statement.executeQuery();

        while(rs.next())
        {
            messages.add(
                    new Message(
                    rs.getString("owner"),
                    rs.getDate("createdOn"),
                    rs.getBoolean("personal"),
                    rs.getString("content"),
                    rs.getString("id"))
            );
        }
        return messages;
    }
    
    public List<Message> searchMessages(String text, String owner) throws SQLException
    {
        List<Message> messages = new ArrayList<>();
        PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM message WHERE content LIKE ? AND (owner=? OR personal=FALSE)");
        statement.setString(1, "%"+text+"%");
        statement.setString(2, owner);
        ResultSet rs = statement.executeQuery();

        while(rs.next())
        {
            messages.add(
                    new Message(
                    rs.getString("owner"),
                    rs.getDate("createdOn"),
                    rs.getBoolean("personal"),
                    rs.getString("content"),
                    rs.getString("id"))
            );
        }
        return messages;
    }

    public Map<String, List<Message>> getAllMessages(String owner) throws SQLException
    {
        Map<String, List<Message>> map = new TreeMap<>();
        List<String> owners = getAccounts();
        owners.remove(owner);
        for(String o : getAccounts())
        {
            List<Message> messages = getUserMessages(o);
            messages = messages.stream().filter(message-> !message.isPersonal()).collect(Collectors.toList());
            map.put(o, messages);
        }

        return map;
    }

    private List<String> getAccounts() throws SQLException {
        List<String> owners = new ArrayList<>();

        PreparedStatement statement = connection.prepareStatement("SELECT owner FROM account");
        ResultSet rs = statement.executeQuery();
        while(rs.next())
        {
            owners.add(rs.getString("owner"));
        }

        return owners;
    }

    public boolean createAccount(Account account) throws SQLException, HasAccountException
    {
        if(hasAccount(account.getUsername()))
        {
            throw new HasAccountException();
        }
        PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO account (username,password) VALUES (?,?)");
        statement.setString(1, account.getUsername());
        statement.setString(2, account.getPassword());
        return statement.execute();
    }

    public boolean hasAccount(String username) throws SQLException
    {
        PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM account WHERE username=?");

        statement.setString(1, username);
        ResultSet rs = statement.executeQuery();
        return rs.next();
    }
    
    public boolean hasAccount(String username, String password) throws SQLException
    {
        PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM account WHERE username=? AND password=?");

        statement.setString(1, username);
        statement.setString(2, password);
        ResultSet rs = statement.executeQuery();
        return rs.next();
    }

    public Account getAccount(String username) throws SQLException
    {
        PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM account WHERE username=?");

        statement.setString(1, username);
        ResultSet rs = statement.executeQuery();
        if(rs.next())
        {
            return new Account(rs.getString("username"), 
                    rs.getString("password"));
        }
        
        return null;
    }

    public static Database getInstance(Configuration configuration) throws SQLException, ClassNotFoundException
    {
        if(database == null)
        {
            database = new Database(configuration);
        }

        return database;
    }

    public static Database getInstance()
    {
        return database;
    }

    public void close() throws SQLException 
    {
        connection.close();
    }


    public class HasAccountException extends Exception
    {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

    }
}
