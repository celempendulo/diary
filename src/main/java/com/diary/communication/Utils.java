package com.diary.communication;

import com.diary.database.Database;
import com.diary.entity.Account;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.sql.SQLException;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class Utils
{
    private static final ObjectMapper mapper = new ObjectMapper();

    public static String readInput(HttpServletRequest request) throws IOException
    {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream())))
        {
            String v =  br.lines().collect(Collectors.joining(System.lineSeparator()));
            return v;
        }
    }

    public static Object getMessage(HttpServletRequest request, Class<?> type) throws IOException
    {
        String input  = readInput(request);
        return mapper.readValue(input, type);
    }

    public static void sendError(String errorMessage, int errorCode, HttpServletResponse response) throws IOException
    {
        ObjectNode object = mapper.createObjectNode();
        object.put("success", false);
        object.put("error_code", errorCode);
        object.put("error_message", errorMessage);

        try (Writer writer = response.getWriter())
        {
            writer.write(object.toString());
            writer.flush();
        }
    }

    public static void sendSuccessResponseMessage(HttpServletResponse response, 
            Consumer<ObjectNode> addData) throws IOException
    {
        ObjectNode object = mapper.createObjectNode();
        object.put("success", true);
        if(addData != null)
        {
            addData.accept(object);
        }
        try(Writer writer = response.getWriter())
        {
            writer.write(object.toString());
            writer.flush();
        }
    }

    public static void login(HttpServletRequest request, HttpServletResponse response,  Account account)
    {
        request.getSession().setAttribute("account", account);
        Cookie cookie = new Cookie("username", account.getUsername());
        cookie.setHttpOnly(true);
        cookie.setSecure(true);
        response.addCookie(cookie);
    }

    public static void logout(HttpServletRequest request, HttpServletResponse response)
    {
        request.getSession().removeAttribute("account");
        Cookie cookie = new Cookie("username", "");
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }

    public static boolean isAuthenticated(HttpServletRequest request)
    {
        
        if(request.getSession().getAttribute("account") != null)
        {
            return true;
        }
        String username = getCookie(request, "username");
        try 
        {
            if(username != null)
            {
                Account account = Database.getInstance().getAccount(username);
                if(account != null)
                {
                   request.getSession().setAttribute("account", account);
                   return true;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return false;
    }
    
    private static String getCookie(HttpServletRequest request, String name)
    {
        Cookie[] cookies = request.getCookies();
        if(cookies != null)
        {
            for(Cookie cookie: cookies)
            {
                if(name.equals(cookie.getName()))
                {
                    return cookie.getValue();
                }
            }
        }
        
        return null;
    }
}
