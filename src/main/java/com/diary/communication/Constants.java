package com.diary.communication;

public class Constants
{
    public static class Error
    {
        public static final String USERNAME_TAKEN = "username is already taken";
        public static final String INCORRECT_DETAILS = "username or password incorrect";
        public static final String INTERNAL_ERROR = "internal error, contact support with screenshot of this";
        public static final String MESSAGE_MODIFY_NOT_ALLOWED = "This message can only be modified by tye owner";
        public static final String NOT_AUTHORISED = "Action not authorised";
        public static final String MESSAGE_NOT_FOUND = "Message not found";
        public static final String MESSAGE_SAME_AS_OLD = "Message is same as old message";
    }

    public static class ErrorCode
    {
        public static final int USERNAME_TAKEN = 1000;
        public static final int INCORRECT_DETAILS = 1001;
        public static final int INTERNAL_ERROR = 1002;
        public static final int MESSAGE_MODIFY_NOT_ALLOWED = 1003;
        public static final int NOT_AUTHORISED = 1004;
        public static final int MESSAGE_NOT_FOUND = 1005;
        public static final int MESSAGE_SAME_AS_OLD = 1006;
    }
}
