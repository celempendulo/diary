package com.diary.communication.servlets;


import com.diary.communication.Constants;
import com.diary.communication.Utils;
import com.diary.database.Database;
import com.diary.entity.Account;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class RegisterServlet extends HttpServlet
{
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
        resp.setContentType("application/json");
        try
        {
            Account account = (Account) Utils.getMessage(req, Account.class);
            Database.getInstance().createAccount(account);
            Utils.login(req, resp, account);
            Utils.sendSuccessResponseMessage(resp, (ObjectNode t) -> {
                    t.put("username", account.getUsername());
            });
        }
        catch (Database.HasAccountException exception)
        {
                Utils.sendError(Constants.Error.USERNAME_TAKEN, Constants.ErrorCode.USERNAME_TAKEN, resp);
        }
        catch (SQLException exception)
        {
            Utils.sendError(Constants.Error.INTERNAL_ERROR, Constants.ErrorCode.INTERNAL_ERROR, resp);
            exception.printStackTrace();
        }
    }
}
