/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.diary.communication.servlets;

import com.diary.communication.Constants;
import com.diary.communication.Utils;
import com.diary.database.Database;
import com.diary.entity.Account;
import com.diary.entity.Message;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.io.Writer;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mpendulo
 */
public class SearchMessageServlet extends HttpServlet
{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException 
    {
        resp.setContentType("application/json");
        if(!Utils.isAuthenticated(req))
        {
            Utils.sendError(Constants.Error.NOT_AUTHORISED, Constants.ErrorCode.NOT_AUTHORISED, resp);
            return;
        }
        
        try
        {
            Search search = (Search)Utils.getMessage(req, Search.class);
            Account account = (Account) req.getSession().getAttribute("account");

            List<Message> messages = Database.getInstance().
                    searchMessages(search.getContent(), account.getUsername());
            
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode array = mapper.createArrayNode();
            for(Message message: messages)
            {
                array.add(mapper.writeValueAsString(message));
            }
            
            ObjectNode object = mapper.createObjectNode();
            object.put("success", true);
            object.set("messages", array);
            try(Writer writer = resp.getWriter())
            {
                writer.write(object.toString());
                writer.flush();
            }
        }
        catch(SQLException exception)
        {
            Utils.sendError(Constants.Error.INTERNAL_ERROR, Constants.ErrorCode.INTERNAL_ERROR, resp);
            exception.printStackTrace();
        }
        
        
        
    }
    
    
    public static class Search
    {
        private String content;

        public void setContent(String content) 
        {
            this.content = content;
        }

        public String getContent() 
        {
            return content;
        }
    
    }
    
    
}
