package com.diary.communication.servlets;

import com.diary.communication.Constants;
import com.diary.communication.Utils;
import com.diary.database.Database;
import com.diary.entity.Account;
import com.diary.entity.Message;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;

public class RemoveMessageServlet extends HttpServlet
{
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
        doGet(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
       doGet(req, resp);
    }
    
    

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
        resp.setContentType("application/json");
        if(!Utils.isAuthenticated(req))
        {
            Utils.sendError(Constants.Error.NOT_AUTHORISED, Constants.ErrorCode.NOT_AUTHORISED, resp);
            return;
        }

        Account account = (Account) req.getSession().getAttribute("account");
        IdContainer id = (IdContainer) Utils.getMessage(req, IdContainer.class);
        try
        {
            Message message = Database.getInstance().getMessage(id.getId());
            if(message == null)
            {
                Utils.sendError(Constants.Error.MESSAGE_NOT_FOUND, Constants.ErrorCode.MESSAGE_NOT_FOUND, resp);
                return;
            }
            if(!message.getOwner().equals(account.getUsername()))
            {
                Utils.sendError(Constants.Error.MESSAGE_MODIFY_NOT_ALLOWED,
                        Constants.ErrorCode.MESSAGE_MODIFY_NOT_ALLOWED, resp);
                return;
            }
            Database.getInstance().deleteMessage(id.getId());
            Utils.sendSuccessResponseMessage(resp, (ObjectNode t) -> {
                t.put("id", message.getId());
            });
        }
        catch(SQLException exception)
        {
            Utils.sendError(Constants.Error.INTERNAL_ERROR, Constants.ErrorCode.INTERNAL_ERROR, resp);
            exception.printStackTrace();
        }
    }

    public static class IdContainer
    {
        private String id;

        public String getId()
        {
            return id;
        }

        @SuppressWarnings("unused")
        public void setId(String id)
        {
            this.id = id;
        }
    }
}
