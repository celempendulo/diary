package com.diary.communication.servlets;

import com.diary.communication.Constants;
import com.diary.communication.Utils;
import com.diary.database.Database;
import com.diary.entity.Account;
import com.diary.entity.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javax.json.bind.annotation.JsonbProperty;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;

public class UpdateMessageServlet extends HttpServlet
{
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
        doGet(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
        resp.setContentType("application/json");
        if(!Utils.isAuthenticated(req))
        {
            Utils.sendError(Constants.Error.NOT_AUTHORISED, Constants.ErrorCode.NOT_AUTHORISED, resp);
            return;
        }

        Account account = (Account) req.getSession().getAttribute("account");
        Update update = (Update) Utils.getMessage(req, Update.class);
        try
        {
            Message message = Database.getInstance().getMessage(update.getId());
            if(message == null)
            {
                Utils.sendError(Constants.Error.MESSAGE_NOT_FOUND, Constants.ErrorCode.MESSAGE_NOT_FOUND, resp);
                return;
            }
            if(!message.getOwner().equals(account.getUsername()))
            {
                Utils.sendError(Constants.Error.MESSAGE_MODIFY_NOT_ALLOWED,
                        Constants.ErrorCode.MESSAGE_MODIFY_NOT_ALLOWED, resp);
                return;
            }
            boolean changed = false;
            if(update.getContent() != null && !message.getContent().equals(update.getContent()))
            {
                Database.getInstance().updateMessage(update.getId(), update.getContent());
                message.setContent(update.getContent());
                changed = true;
            }

            if(update.isPersonal() != null && message.isPersonal() != update.isPersonal())
            {
                Database.getInstance().updateMessage(update.getId(), update.isPersonal());
                message.setPersonal(update.isPersonal());
                changed = true;
            }

            if(changed)
            {
                Utils.sendSuccessResponseMessage(resp, (ObjectNode t) -> {
                    try 
                    {
                        t.put("id", message.getId());
                        t.put("message", new ObjectMapper().writeValueAsString(message));
                    } 
                    catch (JsonProcessingException ex) 
                    {
                        Logger.getLogger(UpdateMessageServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
            }
            else
            {
                Utils.sendError(Constants.Error.MESSAGE_SAME_AS_OLD, Constants.ErrorCode.MESSAGE_SAME_AS_OLD, resp);
            }
        }
        catch(SQLException exception)
        {
            Utils.sendError(Constants.Error.INTERNAL_ERROR, Constants.ErrorCode.INTERNAL_ERROR, resp);
            exception.printStackTrace();
        }
    }

    public static class Update
    {
        private String content;
        private Boolean personal;
        private String id;

        @SuppressWarnings("unused")
        public void setContent(String content)
        {
            this.content = content;
        }

        @SuppressWarnings("unused")
        public void setId(String id)
        {
            this.id = id;
        }

        @SuppressWarnings("unused")
        public void setPersonal(Boolean personal)
        {
            this.personal = personal;
        }


        @JsonbProperty(nillable = true, value = "content")
        public String getContent()
        {
            return content;
        }

        @JsonbProperty(nillable = true, value = "personal")
        public Boolean isPersonal()
        {
            return personal;
        }

        public String getId()
        {
            return id;
        }
    }
}
