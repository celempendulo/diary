package com.diary.communication.servlets;

import com.diary.communication.Constants;
import com.diary.communication.Utils;
import com.diary.database.Database;
import com.diary.entity.Account;
import com.diary.entity.Message;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

public class AddMessageServlet extends HttpServlet
{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
        resp.setContentType("application/json");
        if(!Utils.isAuthenticated(req))
        {
            Utils.sendError(Constants.Error.NOT_AUTHORISED, Constants.ErrorCode.NOT_AUTHORISED, resp);
            return;
        }

        Message message = (Message) Utils.getMessage(req, Message.class);
        Account account = (Account) req.getSession().getAttribute("account");
        message.setOwner(account.getUsername());
        message.setCreatedOn(new Date());
        try
        {
            message.setId(Database.getInstance().addMessage(message));
            ObjectMapper mapper = new ObjectMapper();
            String msg = mapper.writeValueAsString(message);
            Utils.sendSuccessResponseMessage(resp, (ObjectNode t) -> {
                t.put("message", msg);
            });
        }
        catch(SQLException exception)
        {
            Utils.sendError(Constants.Error.INTERNAL_ERROR, Constants.ErrorCode.INTERNAL_ERROR, resp);
            exception.printStackTrace();
        }
    }
}
