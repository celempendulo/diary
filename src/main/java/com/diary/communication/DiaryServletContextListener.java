package com.diary.communication;

import com.diary.database.Database;
import com.diary.entity.Configuration;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class DiaryServletContextListener implements ServletContextListener
{
    @Override
    public void contextInitialized(ServletContextEvent event)
    {
        try
        {
            InputStream stream = getClass().getResourceAsStream("/configuration.json");
            Database.getInstance(new ObjectMapper().readValue(stream, Configuration.class));
        }
        catch(IOException | ClassNotFoundException | SQLException exception)
        {
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) 
    {
        if(Database.getInstance() != null)
        {
            try 
            {
                Database.getInstance().close();
            } 
            catch (SQLException ex)
            {
                Logger.getLogger(DiaryServletContextListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
}
