/********
 NETWORK
 *******/

let networkAPI = {

    register: {

        type: "POST",
        url: "/register",
        request: function (element) {


            let username = document.querySelector("#registration-page .username-input").value;
            let password1 = document.querySelector("#registration-page .password-1-input").value;
            let password2 = document.querySelector("#registration-page .password-2-input").value;

            if (password1 !== password2) {

                showNotificationModal({title: "Input Error", text: "Passwords don't match"});
                document.querySelector("#registration-form").reset();
                return;
            }

            return {username: username, password: password1};
        },
        response: function (data) {

            document.querySelector("#registration-page").classList.remove("active");
            document.querySelector("#home-page").classList.add("active");
            sendRequest("searchMessages");
        }
    },

    login: {

        type: "POST",
        url: "/login",
        request: function (element) {

            let username = document.querySelector("#login-page .username-input").value;
            let password = document.querySelector("#login-page .password-input").value;
            return {username: username, password: password};
        },
        response: function () {

            document.querySelector("#login-page").classList.remove("active");
            document.querySelector("#home-page").classList.add("active");
            sendRequest("searchMessages");
        }
    },

    logout: {

        type: "POST",
        url: "/logout",
        request: function (element) {

            return {};
        },
        response: function () {

            document.querySelector("#login-page").classList.add("active");
            document.querySelector("#home-page").classList.remove("active");
        }
    },

    addMessage: {

        type: "POST",
        url: "/add",
        request: function (element) {

            let messageText = document.querySelector("#creator-message-text").value;
            let publicSetting = document.querySelector("#creator-public-setting").checked;
            return {personal: publicSetting, content: messageText};
        },
        response: function (data) {

            let message = JSON.parse(data.message);
            document.querySelector("#messages-list").prepend(renderMessage(message));
            document.querySelector("#home-page").classList.remove("show-creator-modal");
        }
    },

    updateMessage: {

        type: "PUT",
        url: "/update",
        request: function (element) {

            let messageText = document.querySelector("#updater-message-text").value;
            let publicSetting = document.querySelector("#updater-public-setting").checked;
            return {personal: publicSetting, content: messageText, id: element.id.substr(1)};
        },
        response: function (data) {

            let messageEl = document.querySelector("#_" + data.id);
            let message = JSON.parse(data.message);
            if (message.personal === true) messageEl.querySelector(".message-type").textContent = "private message";
            if (message.personal === false) messageEl.querySelector(".message-type").textContent = "public message";
            if (message.content) messageEl.querySelector(".message-text").textContent = message.content;
            document.querySelector("#home-page").classList.remove("show-updater-model");
        }
    },

    removeMessage: {

        type: "DELETE",
        url: "/delete",
        request: function (element) {

            return {id: element.closest(".message").id.substr(1)};
        },
        response: function (data) {

            document.querySelector("#_" + data.id).remove();
        }
    },

    searchMessages: {

        type: "POST",
        url: "/search",
        request: function (element) {

            return {content: document.querySelector("#search-query").value};
        },
        response: function (data) {

            let messages = data["messages"];
            let list = document.querySelector("#messages-list");
            for(let m = 0; m < messages.length; m++) 
            {
                let message = JSON.parse(messages[m]);
                list.prepend(renderMessage(message));
            }
        }
    }
};

function sendRequest(requestName, element) {


    
    let params = networkAPI[requestName].request(element);

    if (!params) return;
    let request = new XMLHttpRequest();
    let options = networkAPI[requestName];
    let successCallback = options.response;

    request.onreadystatechange = function () {
        let response = JSON.parse(this.responseText || "{}");

        if (this.status===200 && this.readyState===4) {
            if(response["success"])
            {
                successCallback(response);
                return;
            }
            
            showNotificationModal({
                title:  response["error_code"] && "Error Code " + response["error_code"] || "Error",
                text: response["error_message"] || "Something went wrong"
            });
        }
    };

    request.open(options.type, options.url, true);
    request.send(JSON.stringify(params));
}


/*******
 ACTIONS
 *******/

function openRegistrationPage(e) {

    document.querySelector("#login-page").classList.remove("active");
    document.querySelector("#registration-page").classList.add("active");
}

function openLoginPage(e) {

    document.querySelector("#registration-form").reset();
    document.querySelector("#login-page").classList.add("active");
    document.querySelector("#registration-page").classList.remove("active");
}

function showMessageCreatorModal(e) {

    document.querySelector("#home-page").classList.add("show-creator-modal");
}

function hideMessageCreatorModal(e) {

    if (e.target !== e.currentTarget) return;
    document.querySelector("#home-page").classList.remove("show-creator-modal");
}

function showMessageUpdaterModal(e) {

    let messageEl = e.target.closest("li.message");
    let publicSetting = messageEl.getAttribute("data-personal");

    document.querySelector("#updater-message-text").textContent = messageEl.querySelector(".message-text").textContent;
    document.querySelector("#updater-public-setting").setAttribute("checked", publicSetting);
    document.querySelector(".update-message-modal .action-button").id=messageEl.id;
    document.querySelector("#home-page").classList.add("show-updater-modal");
}

function hideMessageUpdaterModal(e) {

    if (e.target !== e.currentTarget) return;
    document.querySelector("#home-page").classList.remove("show-updater-modal");
}

function showNotificationModal(data) {


    let titleEl = document.querySelector("#notification-title");
    let textEl = document.querySelector("#notification-text");

    titleEl.textContent = data.title || "Notification";
    textEl.textContent = data.text || "Here is some information";
    document.body.classList.add("show-notification-modal");
}

function hideNotificationModal(e) {

    if (e.target !== e.currentTarget) return;
    document.body.classList.remove("show-notification-modal");
}


/********
 RENDER
 ********/

function renderMessage(data) {

    let element = document.createElement("li");

    element.setAttribute("id", "_"+data.id);
    element.setAttribute("data-personal", data.personal);
    element.classList.add("message");

    element.innerHTML = `
            
        <header>
                <div class="message-by">${data.owner}</div>
                <div class="message-type">${data.personal ? "private" : "public"} message</div>
            </header>
            <p class="message-text">${data.content}</p>
            <div class="toolbar">
                <button type="button" onclick="showMessageUpdaterModal(event)" class="theme icon update-message"></button>
                <button type="button" onclick="sendRequest('removeMessage', this)" class="theme icon delete-message"></button>
            </div>
    `;

    return element;
}

function resetResults(e) {

    if (e.target.value === "") sendRequest("searchMessages", e.target);
}

/********
 Initialize application
 ********/
let cookies = document.cookie && document.cookie.split("=");
if(cookies && cookies[0]==="username")
{
    document.querySelector("#home-page").classList.add("active");
    sendRequest("searchMessages");
}
else
{
    document.querySelector("#login-page").classList.add("active");
}